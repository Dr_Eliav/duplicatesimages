import java.util.Random;

public class MainClass {

  public static final Random rand = new Random();

  public static void main(String[] args) {

    Population population = new Population();
    population.initialization();
    population.calcFitness();

    while (!FileHandler.getTargetFromFile().equals(FileHandler.getResultFromFile())) {
      population.selection();
      population.evaluation();
      population.mutation();
      population.calcFitness();
      population.printPopulation();
    }
  }
}
