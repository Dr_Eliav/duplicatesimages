import java.awt.image.BufferedImage;

public class Chromosome {

  private static final int IMAGE_HEIGHT = 10;
  private static final int IMAGE_WIDTH = 10;
  private static final int MAX_FITNESS = IMAGE_HEIGHT * IMAGE_WIDTH;

  private static final int VALUES[] = {
    -1, -3947581, -10987432, -16777216, -7864293, -1303516, -32985, -13800, -136026, -3584,
    -3866866, -15806139, -7536645, -16733965, -12629812, -4702790, -20792, -4621738
  };

  private BufferedImage image;
  private int fitness;

  public Chromosome() {
    this.image = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
    this.fitness = 0;
  }

  public Chromosome(Chromosome c1, Chromosome c2) {
    this();

    int crossHeight = MainClass.rand.nextInt(IMAGE_HEIGHT) + 1;
    int crossWidth = MainClass.rand.nextInt(IMAGE_WIDTH) + 1;

    for (int row = 0; row < IMAGE_HEIGHT; row++) {
      for (int col = 0; col < IMAGE_WIDTH; col++) {
        if (row < crossHeight && col < crossWidth) {
          this.image.setRGB(col, row, c1.image.getRGB(col, row));
        } else {
          this.image.setRGB(col, row, c2.image.getRGB(col, row));
        }
      }
    }
  }

  public BufferedImage image() {
    return this.image;
  }

  public void setImage(BufferedImage bufferedImage) {
    this.image = bufferedImage;
  }

  public int fitness() {
    return this.fitness;
  }

  private int randomValue() {
    return VALUES[MainClass.rand.nextInt(VALUES.length)];
  }

  public void setRandomValues() {
    for (int row = 0; row < IMAGE_HEIGHT; row++) {
      for (int col = 0; col < IMAGE_WIDTH; col++) {
        this.image.setRGB(col, row, randomValue());
      }
    }
  }

  public void calcFitness(Chromosome target) {
    this.fitness = 0;

    for (int row = 0; row < IMAGE_HEIGHT; row++) {
      for (int col = 0; col < IMAGE_WIDTH; col++) {
        if (this.image.getRGB(col, row) == target.image.getRGB(col, row)) this.fitness++;
      }
    }
  }

  public void mutation() {
    for (int count = 0; count < 3; count++) {
      this.image.setRGB(
          MainClass.rand.nextInt(IMAGE_WIDTH),
          MainClass.rand.nextInt(IMAGE_WIDTH),
          this.randomValue());
    }
  }

  @Override
  public boolean equals(Object obj) {
    boolean isEquals = false;
    if (this == obj) {
      isEquals = true;
    } else if (obj instanceof Chromosome) {
      Chromosome chromosome = (Chromosome) obj;
      isEquals = true;

      for (int row = 0; row < IMAGE_HEIGHT && isEquals; row++) {
        for (int col = 0; col < IMAGE_WIDTH && isEquals; col++) {
          isEquals = (this.image.getRGB(col, row) == chromosome.image.getRGB(col, row));
        }
      }
    }

    return isEquals;
  }
}
