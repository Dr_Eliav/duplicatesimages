import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class FileHandler {

  public static final String EXTENSION = "png";
  private static final String RESULT = "result." + EXTENSION;
  private static final String TARGET = "target3." + EXTENSION;

  public static void saveImageToFile(Chromosome chromosome) {

    try {
      File file = new File(RESULT);
      ImageIO.write(chromosome.image(), EXTENSION, file);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public static Chromosome getTargetFromFile() {
    Chromosome chromosome = new Chromosome();

    try {
      File file = new File(TARGET);
      chromosome.setImage(ImageIO.read(file));
    } catch (IOException ex) {
      ex.printStackTrace();
    }

    return chromosome;
  }

  public static Chromosome getResultFromFile() {
    Chromosome chromosome = new Chromosome();

    try {
      File file = new File(RESULT);
      chromosome.setImage(ImageIO.read(file));
    } catch (IOException ex) {
      ex.printStackTrace();
    }

    return chromosome;
  }
}
