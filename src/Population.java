import java.util.ArrayList;
import java.util.List;

public class Population {

  private static Chromosome target = FileHandler.getTargetFromFile();
  private static final int NUM_POPULATIONS = 1000;
  private static final double MUTATION_PERCENTAGE = 0.02;

  private Chromosome[] chromosomes;
  private List<Chromosome> poolSelection;
  private int generation;
  private int bestFitnessIndex;

  public Population() {
    this.generation = 1;
    this.chromosomes = new Chromosome[NUM_POPULATIONS];

    for (int index = 0; index < NUM_POPULATIONS; index++) {
      chromosomes[index] = new Chromosome();
    }
  }

  private Chromosome getChromosome(int index) {
    return this.chromosomes[index];
  }

  private void setNewChromosome(int index, Chromosome newChromosome) {
    this.chromosomes[index] = newChromosome;
  }

  private Chromosome getFromPoolSelection() {
    return poolSelection.get(MainClass.rand.nextInt(poolSelection.size()));
  }

  private void clearPoolSelection() {
    this.poolSelection = new ArrayList<Chromosome>();
  }

  private void addToPoolSelection(Chromosome toAdd) {
    this.poolSelection.add(toAdd);
  }

  public void addGeneration() {
    this.generation++;
  }

  public int generation() {
    return this.generation;
  }

  private void changeBestFitnessIndex(int index) {
    this.bestFitnessIndex = index;
  }

  private void resetBestFitnessIndex() {
    this.bestFitnessIndex = 0;
  }

  public int bestFitnessIndex() {
    return this.bestFitnessIndex;
  }

  public void initialization() {
    for (int index = 0; index < NUM_POPULATIONS; index++) {
      this.getChromosome(index).setRandomValues();
    }
  }

  private void calcBestFitness() {
    this.resetBestFitnessIndex();

    for (int index = 1; index < NUM_POPULATIONS; index++) {
      if (this.getChromosome(index).fitness()
          > this.getChromosome(this.bestFitnessIndex()).fitness()) {
        this.changeBestFitnessIndex(index);
      }
    }

    FileHandler.saveImageToFile(this.getChromosome(this.bestFitnessIndex()));
  }

  public void calcFitness() {
    for (int index = 0; index < NUM_POPULATIONS; index++) {
      this.getChromosome(index).calcFitness(target);
    }
    this.calcBestFitness();
  }

  public void selection() {
    this.clearPoolSelection();

    for (int index = 0; index < NUM_POPULATIONS; index++) {
      for (int count = 0; count < this.getChromosome(index).fitness(); count++) {
        this.addToPoolSelection(this.getChromosome(index));
      }
    }
  }

  public void evaluation() {

    for (int index = 0; index < NUM_POPULATIONS; index++) {
      Chromosome partnerA = this.getFromPoolSelection();
      Chromosome partnerB = this.getFromPoolSelection();
      this.setNewChromosome(index, new Chromosome(partnerA, partnerB));
    }

    this.addGeneration();
  }

  public void mutation() {
    for (int index = 0; index < NUM_POPULATIONS * MUTATION_PERCENTAGE; index++) {
      this.getChromosome(MainClass.rand.nextInt(NUM_POPULATIONS)).mutation();
    }
  }

  public void printPopulation() {
    System.out.println("Generation: " + this.generation());
    System.out.println("Best fitness: " + this.getChromosome(this.bestFitnessIndex()).fitness());
  }
}
